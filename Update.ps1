﻿﻿
$PSDefaultParameterValues = @{ '*:Encoding' = 'utf8' }
if (-NOT ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))  
{  
    Start-Process powershell -Verb runAs -ArgumentList "-Command (New-Object Net.WebClient).Proxy.Credentials=[Net.CredentialCache]::DefaultNetworkCredentials;iwr('http://net.ksg.lt/SEM/setup/Update.ps1')|iex"
    Break
    exit
}


$path = "C:\Program Files\SEM"
If(!(test-path $path))
{
    New-Item -ItemType Directory -Force -Path $path
}

$path = "C:\Program Files\SEM\screenshot"
If(!(test-path $path))
{
    New-Item -ItemType Directory -Force -Path $path
    $Acl = Get-Acl $path
    $Ar = New-Object System.Security.AccessControl.FileSystemAccessRule("users", "FullControl", "ContainerInherit,ObjectInherit", "None", "Allow")
    $Acl.SetAccessRule($Ar)
    Set-Acl $path $Acl
}

$colors = @("MyMonitor.ps1","Update.ps1","MyMonitor.bat","Update.bat")
For ($i=0; $i -lt $colors.Length; $i++) {
    $url = "https://net.ksg.lt/SEM/setup/?file=$($colors[$i])&PCID=$((Get-CimInstance -Class Win32_ComputerSystemProduct).UUID)"
    Write-Output "[$(Get-Date)] [*] Downloading $($colors[$i]) from $url"	
	$client = new-object System.Net.WebClient
	$client.Encoding = [System.Text.Encoding]::UTF8
	$client.DownloadFile($url,"C:\Program Files\SEM\$($colors[$i])")
    Sleep 2
}

Write-Output "[$(Get-Date)] Removing old task."
&schtasks /delete /tn MyMonitor /f
&schtasks /delete /tn SEM /f
&schtasks /delete /tn SEMUpdate /f
&schtasks /delete /tn SEMUpdate2 /f
&schtasks /delete /tn BCloud /f


Write-Output "[$(Get-Date)] Create update new task."
try {
	$D = New-ScheduledTask `
		-Action $(New-ScheduledTaskAction -Execute "C:\Program Files\SEM\Update.bat") `
        -Principal $(New-ScheduledTaskPrincipal "SYSTEM") `
		-Trigger $(New-ScheduledTaskTrigger -Daily -At 1AM) `
		-Settings $(New-ScheduledTaskSettingsSet -StartWhenAvailable -AllowStartIfOnBatteries -DontStopIfGoingOnBatteries)
    Register-ScheduledTask SEMUpdate -InputObject $D
} catch {
	&schtasks /Create /RU SYSTEM /TN SEMUpdate /SC DAILY /ST 09:00 /TR "C:\Program Files\SEM\Update.bat" /F
}

Write-Output "[$(Get-Date)] Add to startup."
$WshShell = New-Object -comObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut("$($Env:ALLUSERSPROFILE)\Microsoft\Windows\Start Menu\Programs\StartUp\SEM.lnk")
$Shortcut.TargetPath = "C:\Program Files\SEM\MyMonitor.bat"
$Shortcut.WorkingDirectory = "C:\Program Files\SEM\"
$Shortcut.Save()

Write-Output "[$(Get-Date)] Update Don